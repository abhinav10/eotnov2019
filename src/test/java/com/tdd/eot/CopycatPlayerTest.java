package com.tdd.eot;

import org.junit.Test;

import static com.tdd.eot.Move.CHEAT;
import static com.tdd.eot.Move.COOPERATE;
import static org.junit.Assert.assertEquals;

public class CopycatPlayerTest {

    @Test
    public void shouldReturnCooperateAsTheFirstMove(){
        CopycatPlayer copycatPlayer = new CopycatPlayer();
        assertEquals(COOPERATE, copycatPlayer.makeMove());
    }

    @Test
    public void shouldReturnPreviousPlayersMoveIfCopycatIsFirstPlayer(){
        CopycatPlayer copycatPlayer = new CopycatPlayer();
        copycatPlayer.makeMove();
        copycatPlayer.update(COOPERATE, CHEAT);
        assertEquals(CHEAT, copycatPlayer.makeMove());
    }

    @Test
    public void shouldReturnPreviousPlayersMoveIfCopycatIsSecondPlayer(){
        CopycatPlayer copycatPlayer = new CopycatPlayer();
        copycatPlayer.makeMove();
        copycatPlayer.update(CHEAT, COOPERATE);
        assertEquals(CHEAT, copycatPlayer.makeMove());
    }

}
