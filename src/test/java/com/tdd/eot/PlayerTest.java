package com.tdd.eot;

import org.junit.Test;

import static com.tdd.eot.Move.CHEAT;
import static com.tdd.eot.Move.COOPERATE;
import static org.junit.Assert.*;

public class PlayerTest {

    @Test
    public void shouldMakeTheFirstMovePassedInTheConstructor() {
        Player player = new Player(CHEAT);
        assertEquals(CHEAT, player.makeMove());
    }

    @Test
    public void shouldMakeTheSecondAndThirdMovesPassedInTheConstructor() {
        Player player = new Player(CHEAT, CHEAT, COOPERATE);
        player.makeMove();
        assertEquals(CHEAT, player.makeMove());
        assertEquals(COOPERATE, player.makeMove());
    }
}