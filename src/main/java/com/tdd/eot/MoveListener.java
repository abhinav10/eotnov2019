package com.tdd.eot;

public interface MoveListener {
    void update(Move player1Move, Move player2Move);
}
