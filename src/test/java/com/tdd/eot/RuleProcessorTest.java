package com.tdd.eot;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RuleProcessorTest {
    private RuleProcessor ruleProcessor = new RuleProcessor();

    @Test
    public void shoudReturnScoreWhenBothPlayersCheat(){
        Score score = ruleProcessor.process(Move.CHEAT, Move.CHEAT);
        assertEquals(new Score(0,0), score);
    }

    @Test
    public void shoudReturnScoreWhenBothPlayersCooperate(){
        Score score = ruleProcessor.process(Move.COOPERATE, Move.COOPERATE);
        assertEquals(new Score(2,2), score);
    }

    @Test
    public void shoudReturnScoreWhenOnlyFirstPlayerCheats(){
        Score score = ruleProcessor.process(Move.CHEAT, Move.COOPERATE);
        assertEquals(new Score(3,-1), score);
    }

    @Test
    public void shoudReturnScoreWhenOnlySecondPlayerCheats(){
        Score score = ruleProcessor.process(Move.COOPERATE, Move.CHEAT);
        assertEquals(new Score(-1,3), score);
    }

    @Test(expected = RuntimeException.class)
    public void shouldThrowRuntimeExceptionIfAnyMoveIsNull(){
        Score score = ruleProcessor.process(null, Move.CHEAT);
    }
}
