package com.tdd.eot;

import java.util.ArrayList;
import java.util.List;

public class Game {
    private final Player player1;
    private final Player player2;
    private final RuleProcessor ruleProcessor;
    private List<MoveListener> moveListeners = new ArrayList<>();

    public Game(Player player1, Player player2, RuleProcessor ruleProcessor) {
        this.player1 = player1;
        this.player2 = player2;
        this.ruleProcessor = ruleProcessor;
    }

    public Score play(int rounds) {
        Score score = new Score(0,0);
        for (int currentRound = 0; currentRound < rounds; currentRound++) {
            final Move player1Move = player1.makeMove();
            final Move player2Move = player2.makeMove();
            score = score.add(ruleProcessor.process(player1Move, player2Move));
            moveListeners.forEach(moveListener -> moveListener.update(player1Move, player2Move));
        }
        return score;
    }

    public void addMoveListener(MoveListener moveListener) {
        this.moveListeners.add(moveListener);
    }
}
