package com.tdd.eot;

import org.junit.Test;

import static com.tdd.eot.Move.CHEAT;
import static org.junit.Assert.assertEquals;

public class CheatingPlayerTest {
    @Test
    public void shouldAlwaysCheat() {
        CheatingPlayer cheatingPlayer = new CheatingPlayer();
        assertEquals(CHEAT, cheatingPlayer.makeMove());
    }}
