package com.tdd.eot;

import java.util.Arrays;
import java.util.Iterator;

public class Player {
    private final Iterator<Move> movesIterator;

    public Player(Move... moves) {
        this.movesIterator = Arrays.asList(moves).iterator();
    }

    public Move makeMove() {
        return movesIterator.next();
    }
}
