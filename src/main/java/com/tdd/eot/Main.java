package com.tdd.eot;

public class Main {
    public static void main(String[] args){
        final Game game = new Game(new ConsolePlayer(new Console()), new CheatingPlayer(), new RuleProcessor());
        final Score score = game.play(3);
        System.out.println(score);
    }
}
