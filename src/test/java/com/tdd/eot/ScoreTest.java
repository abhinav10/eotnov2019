package com.tdd.eot;

import org.junit.Test;

import static org.junit.Assert.*;

public class ScoreTest {

    @Test
    public void shouldAddAnotherScore() {
        Score score = new Score(1, 2);
        Score other = new Score(4,5);
        assertEquals(new Score(5, 7), score.add(other));
    }
}